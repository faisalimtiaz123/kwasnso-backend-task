const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const apiRouter = require("./app/routes/apiRouter");
const app = express();
app.use(express.json());
app.use(
  cors({
    origin: ["http://localhost:3000"],
  })
);
app.use(express.urlencoded({ extended: false }));
app.use("/", apiRouter);

mongoose
  .connect("mongodb://localhost:27017/kwansoTaskDB")
  .then(() => console.log("MongoDB Connected..."))
  .catch((err) => console.log(err));

var port = "5000";

app.listen(port, () => {
  console.log("server listening on port ", port);
});

module.exports = app;
