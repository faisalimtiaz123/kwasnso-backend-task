const User = require("../Models/User");

const registerUser = async (email, hashedPassword) => {
  const newUser = new User({
    email: email,
    password: hashedPassword,
  });
  const user = await newUser.save();
  return user;
};

const findUser = async (email) => {
  const user = await User.find({ email: email });
  return user;
};

module.exports = {
  registerUser,
  findUser,
};
