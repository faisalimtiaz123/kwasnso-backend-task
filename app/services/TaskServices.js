const Task = require("../Models/Task");

const createTask = async (name) => {
  const newTask = new Task({
    name: name,
  });
  const task = await newTask.save();
  return task;
};

const getAllTasks = async () => {
  const tasks = await Task.find();
  return tasks;
};

module.exports = {
  getAllTasks,
  createTask,
};
