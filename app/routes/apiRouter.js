const express = require("express");

const taskController = require("../../app/controllers/TaskController");
const userController = require("../../app/controllers/UserController");

const apiRouter = express.Router();

apiRouter.route("/register").post(userController.register);
apiRouter.route("/login").post(userController.login);
apiRouter.route("/user").get(userController.verifyJWT, userController.user);
apiRouter
  .route("/create-task")
  .post(userController.verifyJWT, taskController.createTask);
apiRouter
  .route("/list-tasks")
  .get(userController.verifyJWT, taskController.listTasks);

module.exports = apiRouter;
