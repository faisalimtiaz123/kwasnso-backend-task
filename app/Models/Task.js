const { Schema, model } = require("mongoose");

const TaskSchema = Schema({
  Name: { type: String, required: true },
});

const Task = model("Task", TaskSchema);

module.exports = Task;
