const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const userServices = require("../services/UserServices");

async function decodeToken(req) {
  try {
    let token = req.headers.authorization;
    if (token) {
      if (token.startsWith("Bearer")) {
        token = token.slice(7);
      }
      // in Real World app, we will store secret key in .env
      const decodedToken = jwt.verify(token, "kwanso");
      return decodedToken;
    } else {
      return null;
    }
  } catch (e) {
    console.log("error", e);
  }
}

module.exports.user = async (req, res) => {
  try {
  } catch (e) {
    console.log("error", e);
  }
};

module.exports.register = async (req, res) => {
  try {
    const { email, password } = req.body;

    const hashedPassword = await bcrypt.hash(password, 10);

    const result = userServices.registerUser(email, hashedPassword);
    res.status(201).json({ user: result });
  } catch (e) {
    console.log("error", e);
  }
};

module.exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;

    const user = userServices.findUser(email);
    if (user === null) {
      return res.status(400).json({ msg: "User not Exist" });
    }
    if (bcrypt.compare(password, user.password)) {
      const payload = {
        id: user.id,
        email: user.email,
      };
      // in Real World app, we will store secret key in .env
      const token = jwt.sign(payload, "kwanso");
      res.status(201).json({ jwt: token });
    }
  } catch (e) {
    console.log("error", e);
  }
};

module.exports.verifyJWT = async (req, res, next) => {
  try {
    const decodedToken = await decodeToken(req);

    if (!decodedToken) {
      res.status(401).json({
        msg: "not verified",
      });
    } else {
      next();
    }
  } catch (e) {
    console.log("error", e);
  }
};
