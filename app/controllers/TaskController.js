const taskServices = require("../services/TaskServices");

module.exports.createTask = async (req, res) => {
  try {
    const { name } = req.body;
    if (!name) {
      return res.status(204).json({ msg: "Task Name is empty" });
    }
    const result = taskServices.createTask(name);
    res.status(201).json({ task: result });
  } catch (e) {
    console.log("error", e);
  }
};

module.exports.listTasks = async (req, res) => {
  try {
    const result = taskServices.getAllTasks();
    res.status(200).json({ tasks: result });
  } catch (e) {
    console.log("error", e);
  }
};
